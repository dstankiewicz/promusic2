/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.gerber;

import java.io.*;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author nathan
 */
class FileParser {

    File file;
    public FileParser(File file) {
        this.file = file;
    }
    
    ArrayList<Track> parseTracks() {
        ArrayList<Track> trackList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(this.file))) {
            String line = br.readLine();
            while ((line = br.readLine()) != null) {
                String[] split = line.split(":");
                Track temp = new Track(split[0], split[1] + ":" + split[2]);
                trackList.add(temp);
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        return trackList;
        
    }
}

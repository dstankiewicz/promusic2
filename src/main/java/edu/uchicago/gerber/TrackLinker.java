/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.uchicago.gerber;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import java.util.List;
import java.util.concurrent.Callable;

/**
 *
 * @author nathan
 */
public class TrackLinker implements Callable<String> {

    Track trk;

    public TrackLinker(Track trk) {
        this.trk = trk;
    }

    @Override
    public String call() throws Exception {
        
        String youtubeMP3 = "http://www.youtube-mp3.org";
        String youtubeURL = trk.getTrackURL();

        final WebClient webClient = new WebClient();
        // Make the htmlunit code stop spitting out all the warnings for every 'cookie' and external javascript function
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(java.util.logging.Level.OFF);
        webClient.setJavaScriptErrorListener(null);
        try {
            // get youtube-mp3.org
            final HtmlPage page = webClient.getPage(youtubeMP3);
            // get the form where we input the youtube link
            final HtmlForm form = (HtmlForm) page.getElementById("submit-form");
            // get the actual input txtField
            final HtmlElement txtYoutubeURL = (HtmlElement) page.getElementById("youtube-url");
            // click to make sure we activate it
            txtYoutubeURL.click();
            // type in the URL
            txtYoutubeURL.type(youtubeURL);
            // grab the "Convert Video" button
            final HtmlSubmitInput button = form.getInputByValue("Convert Video");
            // grab the download page after we click
            final HtmlPage download = button.click();
            
            // execute the javaScript necessary to get passed "In Progress.."
            download.executeJavaScript("init");
            download.executeJavaScript("loadAds");
            download.executeJavaScript("acb");
            webClient.waitForBackgroundJavaScript(2000);

            // grab the div element with the download link(s)
            final HtmlDivision download_link_div = download.getHtmlElementById("dl_link");
            
            // grab the actual anchors in the div element
            final List<HtmlAnchor> anchors = (List<HtmlAnchor>) download.getByXPath("//div[@id='dl_link']//a");
            String link_text = "";
            for (HtmlAnchor anchor : anchors) {
                // some detective work resulted in learning that the actual video is stored through the anchor with /get?ab=...
                if (anchor.getHrefAttribute().contains("/get?ab=")) {
                    link_text = anchor.getHrefAttribute();
                }
            }
            // if we didn't get the anchor, then something screwy happened
            if (link_text.equals("")) {
                throw new Exception("Could not find mp3");
            }

            // called to close all windows at end of session
            webClient.closeAllWindows();
            // this is to pass the track name up to the Down Tasks so they can post the track name/filename
            return trk.getTrackName() + "::" + youtubeMP3 + link_text;
        } catch (java.lang.IllegalArgumentException e) {
        } catch (com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException e) {
        } catch (com.gargoylesoftware.htmlunit.ScriptException e) {  
            webClient.closeAllWindows();
            return this.call();
        } finally {
            webClient.closeAllWindows();
        }
        return "";
    }

}

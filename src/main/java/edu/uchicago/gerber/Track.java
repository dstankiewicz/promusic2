/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.gerber;

/**
 *
 * @author nathan
 */
     public class Track {
        private String trackName;
        private String trackURL;
        public Track(String name, String url) {
            this.trackName = name;
            this.trackURL = url;
        }

        public String getTrackName() {
            return trackName;
        }

        public void setTrackName(String trackName) {
            this.trackName = trackName;
        }

        public String getTrackURL() {
            return trackURL;
        }

        public void setTrackURL(String trackURL) {
            this.trackURL = trackURL;
        }
        
        
    }

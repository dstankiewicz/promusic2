package edu.uchicago.gerber;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 http://www.programmableweb.com/
 http://www.faroo.com/
 http://www.faroo.com/hp/api/api.html
 http://www.faroo.com/api?q=iphone&start=1&length=10&l=en&src=web&f=json
 http://jsonviewer.stack.hu/
 */
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import edu.uchicago.gerber.yelp.HttpDownUtil;
import edu.uchicago.gerber.yelp.YelpSearchResults;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * FXML Controller class
 *
 * @author ag
 */
public class MP3Controller implements Initializable {

    @FXML
    private TextField txtSearch;

    @FXML
    private Button btnGo;
    @FXML
    private ListView<String> lstView;
    @FXML
    private Label lblStatus;

    @FXML
    private Label lblTitle;

    YelpSearchResults yelpSearchResultLocal;
    File srcTxtFile;

    @FXML
    private TableView<DownTask> table;

    private String strDirSave;
    @FXML
    private Button btnSelect;

    ArrayList<Track> tracks;

    @FXML
    private Button btnStop;

    @FXML
    private TextField txtDownloadDirectory;
    @FXML
    private ProgressBar prgFoundLinks;

    @FXML
    private void btnSelect_go(ActionEvent event) {

        DirectoryChooser directoryChooser = new DirectoryChooser();

        directoryChooser.setTitle("Choose the directory to save your music files");

        //Show open file dialog
        File file = directoryChooser.showDialog(null);

        if (file != null) {

            txtDownloadDirectory.setText(file.getPath());
            strDirSave = file.getAbsolutePath();

        }

    }

    public static String convertSpacesToPluses(String strOrig) {
        return strOrig.trim().replace(" ", "+");
    }

    @FXML
    private void btnSrcFile_go(ActionEvent event) {

        FileChooser fileChooser = new FileChooser();

        fileChooser.setTitle("Choose the file which has the requisite names and links");

        srcTxtFile = fileChooser.showOpenDialog(null);

        if (srcTxtFile != null) {

            txtSearch.setText(srcTxtFile.getPath());
            tracks = new FileParser(srcTxtFile).parseTracks();
            ArrayList<String> trackList = new ArrayList<>();
            for (Track trk : tracks) {

                trackList.add(trk.getTrackName());
            }
            lstView.setItems(FXCollections.observableArrayList(trackList));

        }

    }

    public class GetReviewsTask extends Task<ObservableList<String>> {

        private String convertSpacesToPluses(String strOrig) {
            return strOrig.trim().replace(" ", "+");
        }

        @Override
        protected ObservableList<String> call() throws Exception {

            ObservableList<String> sales = FXCollections.observableArrayList();
            updateMessage("Trying to download your mp3s  . . .");
            // Multithread the actual finding of the mp3 links
            ExecutorService pool = Executors.newFixedThreadPool(3); // creates a pool of threads for the Future to draw from
            CompletionService<String> completionService = new ExecutorCompletionService<String>(pool);

            String youtubeMP3 = "http://www.youtube-mp3.org";
            ArrayList<String> strResults = new ArrayList<>();
            try {
                for (Track trk : tracks) {
                    String youtubeURL = trk.getTrackURL();
                    completionService.submit(new TrackLinker(trk));
                }
                Double numLinks = 0.0;
                for (Track trk : tracks) {

                    Future<String> future_dl_link = completionService.take();
                    System.out.println("Grabbed a download link");
                    numLinks += 1.0;
                    
                    this.updateProgress(numLinks , (double) tracks.size());

                    // only add an actual String to strResults
                    strResults.add(future_dl_link.get());
                    
                    if (strResults == null || strResults.size() == 0) {
                        updateMessage("Could not successfully download one of your files...try again");
                    } else {
                        updateMessage("Found your mp3!");
                    }
                }
                //}
            } catch (Exception e) {
                e.printStackTrace();
            }

            return FXCollections.observableArrayList(strResults);

        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        strDirSave = null;

        // table = new TableView<DownTask>();
        TableColumn<MP3Controller.DownTask, String> statusCol = new TableColumn("Status");
        statusCol.setCellValueFactory(new PropertyValueFactory<MP3Controller.DownTask, String>(
                "message"));
        statusCol.setPrefWidth(100);

        TableColumn<MP3Controller.DownTask, Double> progressCol = new TableColumn("Progress");
        progressCol.setCellValueFactory(new PropertyValueFactory<MP3Controller.DownTask, Double>(
                "progress"));

        progressCol.setPrefWidth(125);

        //this is the most important call
        progressCol
                .setCellFactory(ProgressBarTableCell.<MP3Controller.DownTask>forTableColumn());

        TableColumn<MP3Controller.DownTask, String> fileCol = new TableColumn("File");
        fileCol.setCellValueFactory(new PropertyValueFactory<MP3Controller.DownTask, String>(
                "title"));
        fileCol.setPrefWidth(375);

        //add the cols
        table.getColumns().addAll(statusCol, progressCol, fileCol);

        // Moved the getReviewsTask out here so the stop button has a reference to stop
        final Task<ObservableList<String>> getReviewsTask = new GetReviewsTask();
        btnStop.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                getReviewsTask.cancel(true);
            }
        });
        btnGo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                if (strDirSave == null) {
                    System.err.println("You need to set an output dir!");
                    return;
                }
                
                prgFoundLinks.setVisible(true);
                prgFoundLinks.progressProperty().bind(getReviewsTask.progressProperty());

                table.getItems().clear();
                lblStatus.textProperty().bind(getReviewsTask.messageProperty());
                btnGo.disableProperty().bind(getReviewsTask.runningProperty());

                getReviewsTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                    @Override
                    public void handle(WorkerStateEvent t) {

                        ObservableList<String> observableList = getReviewsTask.getValue();
                        if (observableList == null || observableList.size() == 0) {
                            return;
                        }

                        //add Tasks to the table
                        for (String str : getReviewsTask.getValue()) {
                            String[] strArr = str.split("::");
                            //System.out.println("STR ARR IS THE FOLLOWING::::");
                            //System.out.println(strArr[0]);
                            table.getItems().add(new DownTask(strArr[1], strDirSave + "/" + convertSpacesToPluses(strArr[0])));
                        }

                        //fire up executor-service with limted number of threads
                        ExecutorService executor = Executors.newFixedThreadPool(3, new ThreadFactory() {
                            @Override
                            public Thread newThread(Runnable r) {
                                Thread t = new Thread(r);
                                t.setDaemon(true);
                                return t;
                            }
                        });

                        for (MP3Controller.DownTask pbarTask : table.getItems()) {
                            executor.execute(pbarTask);
                        }

                    }
                });
                new Thread(getReviewsTask).start();

            }
        });

    }

    class DownTask extends Task<Void> {

        private String mp3From, mp3To;

        public DownTask(String mp3From, String mp3To) {
            this.mp3From = mp3From;
            this.mp3To = mp3To;

        }

        @Override
        protected Void call() throws Exception {

            HttpDownUtil util = new HttpDownUtil();
            try {

                this.updateProgress(ProgressIndicator.INDETERMINATE_PROGRESS, 1);
                this.updateMessage("Waiting...");
                util.downloadFile(this.mp3From);

                InputStream inputStream = util.getInputStream();
                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(mp3To + ".mp3");
                this.updateTitle(mp3To  + ".mp3");

                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                long totalBytesRead = 0;
                int percentCompleted = 0;
                long fileSize = util.getContentLength();

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                    totalBytesRead += bytesRead;
                    percentCompleted = (int) (totalBytesRead * 100 / fileSize);
                    updateProgress(percentCompleted, 100);
                }

                updateMessage("done");
                outputStream.close();
                util.disconnect();

            } catch (Exception ex) {
                ex.printStackTrace();
                this.cancel(true);
                table.getItems().remove(this);

            }

            return null;
        }

    }

    //this is a UI-thread-blocking operation and should be called in background thread
    public String getJSON(String url, int timeout) {
        try {
            URL u = new URL(url);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
            // Logger.getLogger(DebugServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            // Logger.getLogger(DebugServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}

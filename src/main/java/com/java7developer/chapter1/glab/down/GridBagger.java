/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.java7developer.chapter1.glab.down;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

/**
 *
 * @author ag
 */
//http://stackoverflow.com/questions/13031637/adding-jpanel-to-jscrollpane
public class GridBagger {

    static int nY = 0;
    protected final static ArrayList<JPanel> jpanels = new ArrayList<JPanel>();

    protected final static JPanel mainPanel = new JPanel();

    private  class SimulateProgressWorker extends SwingWorker<Void, Integer> {

        private int nC;
        private Random rand;
        private JProgressBar bar;
        private JPanel smallPanel, mainPanel;

        public SimulateProgressWorker(JProgressBar bar, JPanel smallPanel) {
            this.bar = bar;
            nC = 0;
            rand = new Random();
            this.smallPanel = smallPanel;
            
              bar.setVisible(true);        
        bar.setStringPainted(true);
        bar.setValue(0);
       // setProgress(0);

        }

        @Override
        protected Void doInBackground() throws Exception {

            if (nC <= 100) {
                Thread.sleep(rand.nextInt(50));
               
               // break;
              
                //nC++;
            }
         
           publish(nC++);
            return null;
        }

        @Override
        protected void done() {
           // super.done(); //To change body of generated methods, choose Tools | Templates.

            if (jpanels.size() > 0) {

                mainPanel.remove(smallPanel);
            }

            mainPanel.updateUI();
        }

        @Override
        protected void process(final List<Integer> chunks) {
            //super.process(chunks); //To change body of generated methods, choose Tools | Templates.
            // int nVal = chunks.get(0)
            bar.setValue(chunks.get(0));

           // setProgress(chunks.get(0));
//            SwingUtilities.invokeLater(new Runnable() {
//                public void run() {
//                 setProgress(chunks.get(0));
//                  //  bar.setValue(chunks.get(0));
//
//                }
//            });

        }

    }
    
//    private void createAddWorkers(){
//              for (int i = 0; i < 30; i++) {
//            new AddWorker(jpanels, mainPanel).execute();
//          // new RemoveWorker(jpanels, mainPanel).execute();
//
//        }
//    }

    private  static class AddWorker extends SwingWorker<Void, Void> {

        private ArrayList<JPanel> jpanels;
        private JPanel mainPanel;
        private Random rand;

        public AddWorker(ArrayList<JPanel> jpanels, JPanel mainPanel) {

            this.jpanels = jpanels;
            this.mainPanel = mainPanel;
            rand = new Random();
        }

        @Override
        protected Void doInBackground() throws Exception {
           // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

            Thread.sleep(rand.nextInt(10 * 1000));

            return null;
        }

        @Override
        protected void done() {
            // super.done(); //To change body of generated methods, choose Tools | Templates.
            GridBagConstraints gbc = new GridBagConstraints();
            JLabel label = new JLabel("Progress (" + nY + ")");
            //JTextField text = new JTextField(15);
            JProgressBar progresBar = new JProgressBar();

            //create a new JPanel and add components to this
            JPanel mini = new JPanel();
            mini.add(label);
            mini.add(progresBar);
            jpanels.add(mini);

            gbc.gridx = GridBagConstraints.FIRST_LINE_START;
            gbc.gridy = nY++;

            mainPanel.add(jpanels.get(jpanels.size() - 1), gbc);
            mainPanel.updateUI();
            
        }

    }

    private static class RemoveWorker extends SwingWorker<Void, Void> {

        private ArrayList<JPanel> jpanels;
        private JPanel mainPanel;
        private Random rand;

        public RemoveWorker(ArrayList<JPanel> jpanels, JPanel mainPanel) {

            this.jpanels = jpanels;
            this.mainPanel = mainPanel;
            rand = new Random();
        }

        @Override
        protected Void doInBackground() throws Exception {
           // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

            Thread.sleep(rand.nextInt(10 * 2000));

            return null;
        }

        @Override
        protected void done() {
            if (jpanels.size() > 0) {
                JPanel removePanel = jpanels.remove(0);
                mainPanel.remove(removePanel);
            }

            mainPanel.updateUI();
        }

    }

    public static void main(String args[]) {
        final JFrame frame = new JFrame("SpringLayout");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = frame.getContentPane();
        GridBagLayout layout = new GridBagLayout();

        JButton next = new JButton("Add components");
        next.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

                GridBagConstraints gbc = new GridBagConstraints();
                JLabel label = new JLabel("Enter Name (" + nY + ")");
                JTextField text = new JTextField(15);
                //create a new JPanel and add components to this
                JPanel mini = new JPanel();
                mini.add(label);
                mini.add(text);
                jpanels.add(mini);

                gbc.gridx = GridBagConstraints.FIRST_LINE_START;
                gbc.gridy = nY++;

                mainPanel.add(jpanels.get(jpanels.size() - 1), gbc);
                mainPanel.updateUI();
                
               // new SimulateProgressWorker(progresBar, mini).execute();

            }
        });

        JButton remove = new JButton("remove random ");
        remove.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                if (jpanels.size() > 0) {
                    JPanel removePanel = jpanels.remove(0);
                    mainPanel.remove(removePanel);
                }

                mainPanel.updateUI();

            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(next);
        buttonPanel.add(remove);

        mainPanel.setLayout(layout);
        contentPane.setLayout(new BorderLayout());

        contentPane.add(new JScrollPane(mainPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), BorderLayout.CENTER);
        contentPane.add(buttonPanel, BorderLayout.SOUTH);
        frame.setSize(500, 800);
        frame.setVisible(true);
        
        

        for (int i = 0; i < 30; i++) {
            new AddWorker(jpanels, mainPanel).execute();
           new RemoveWorker(jpanels, mainPanel).execute();

        }

    }
}

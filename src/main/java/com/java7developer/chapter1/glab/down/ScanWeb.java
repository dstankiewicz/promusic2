/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.java7developer.chapter1.glab.down;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class ScanWeb
{
    
    private static class PdfRunner implements Runnable{

        private String pdfFrom, pdfTo;
        
        public PdfRunner(String pdfFrom, String pdfTo) {
            this.pdfFrom = pdfFrom;
            this.pdfTo = pdfTo;
            
        }
        @Override
        public void run() {
            
             HttpDownUtil util = new HttpDownUtil();
            try {

                
                util.downloadFile(this.pdfFrom);
              
               // nFileLength = util.getContentLength();
                InputStream inputStream = util.getInputStream();
                 try ( // opens an output stream to save into file
                         FileOutputStream outputStream = new FileOutputStream(pdfTo + "/" + util.getFileName())) {
                     byte[] buffer = new byte[4096];
                     int bytesRead = -1;
                     long totalBytesRead = 0;
                     int percentCompleted = 0;
                     long fileSize = util.getContentLength();
                     
                     while ((bytesRead = inputStream.read(buffer)) != -1) {
                         outputStream.write(buffer, 0, bytesRead);
                         totalBytesRead += bytesRead;
                         percentCompleted = (int) (totalBytesRead * 100 / fileSize);
                         
                         // publish(percentCompleted);
                         
                     }
                     
                     System.out.println(pdfFrom + " completed downloading successfully.");
                 }
                util.disconnect();
            } catch (IOException ex) {
               // JOptionPane.showMessageDialog(SWorkerNetBeans.this, "Error downloading file: " + ex.getMessage(),
              //          "Error", JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
              //  setProgress(0);
               // cancel(true);
            }

           // return strLocalSaveToPath + "//" + util.getFileName();
        }
        
    }
    
public static void main (String args[])
{
    Document doc;
    try{
        
        doc =  Jsoup.connect("https://www.google.com/search?q=business+model+innovation+filetype:pdf&lr=&hl=en&ie=UTF-8&source=lnms&sa=X&ei=sZLOU9r3LMyUyAS07oDgDA&ved=0CAQQ_AU#hl=en&lr=&q=business+model+innovation+filetype:pdf").userAgent("Mozilla").ignoreHttpErrors(true).timeout(0).get();
       // Elements links = doc.select("li[class=g]");
        
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Elements hrefs = doc.select("a[href]");
        for (Element href : hrefs) {
            
            String strRef = href.attr("abs:href");
            if (strRef.contains(".pdf") && strRef.contains("https://www.google.com/url?q=")){
                
                  
                String strPdfToDownload = strRef.substring(strRef.indexOf("https://www.google.com/url?q=") + 29, strRef.indexOf(".pdf") + 4);
                
                executorService.execute(new PdfRunner(strPdfToDownload, "c:\\dev\\" ));
               // new Thread(new PdfRunner(strPdfToDownload, "c:\\dev\\" )).start();
                
            }

        }
        
         executorService.shutdown();
    }
    catch (Exception e) {
        e.printStackTrace();
    }
}
}
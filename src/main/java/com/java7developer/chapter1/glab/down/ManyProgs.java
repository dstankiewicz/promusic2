/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.java7developer.chapter1.glab.down;

import static com.java7developer.chapter1.glab.down.GridBagger.jpanels;
import static com.java7developer.chapter1.glab.down.GridBagger.nY;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

//just layout the 10 and (as this is the max size of threadpool any way. And reference them that way. and make them invisible as required. 

/**
 *
 * @author ag
 */
public class ManyProgs extends javax.swing.JFrame {
   static int nY = 0;
   protected final ArrayList<MyBar> myBars = new ArrayList<MyBar>();
   // protected final  ArrayList<JPanel> jpanels = new ArrayList<JPanel>();
    
   //  protected final ArrayList<JProgressBar> progbars = new ArrayList<JProgressBar>();

   // protected final  JPanel mainPanel = new JPanel();
    /**
     * Creates new form ManyProgs
     */
    public ManyProgs() {
        initComponents();
        
         this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = this.getContentPane();
        GridBagLayout layout = new GridBagLayout();

        JButton next = new JButton("Add components");
        next.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

                GridBagConstraints gbc = new GridBagConstraints();
                JLabel label = new JLabel("Enter Name (" + nY + ")");
                JTextField text = new JTextField(15);
                //create a new JPanel and add components to this
                JPanel mini = new JPanel();
                mini.add(label);
                mini.add(text);
                jpanels.add(mini);

                gbc.gridx = GridBagConstraints.FIRST_LINE_START;
                gbc.gridy = nY++;

                mainPanel.add(jpanels.get(jpanels.size() - 1), gbc);
                mainPanel.updateUI();
                
               // new SimulateProgressWorker(progresBar, mini).execute();

            }
        });

        JButton remove = new JButton("remove random ");
        remove.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                if (jpanels.size() > 0) {
                    JPanel removePanel = jpanels.remove(0);
                    mainPanel.remove(removePanel);
                }

                mainPanel.updateUI();

            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(next);
        buttonPanel.add(remove);

        mainPanel.setLayout(layout);
        contentPane.setLayout(new BorderLayout());

        contentPane.add(new JScrollPane(mainPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), BorderLayout.CENTER);
        contentPane.add(buttonPanel, BorderLayout.SOUTH);
       // frame.setSize(500, 800);
     //   frame.setVisible(true);
        
             for (int i = 0; i < 30; i++) {
            new AddWorker(myBars).execute();
           new RemoveWorker(jpanels, mainPanel).execute();

        }
        
        
    }
    
        class SimulateProgressWorker extends SwingWorker<Void, Integer> {

        private int nC;
        private Random rand;
        private ArrayList<MyBar> barAdds;
        private MyBar bar;
        
      

        public SimulateProgressWorker(ArrayList<MyBar> bars, MyBar bar) {
            this.barAdds = bars;
            this.bar = bar;
            nC = 0;
            rand = new Random();
           
            
        // this.bar.setVisible(true);        
      //  this.bar.setStringPainted(true);
       // this.bars.setValue(0);
       // setProgress(0);

        }

        @Override
        protected Void doInBackground() throws Exception {

            if (nC <= 100) {
                Thread.sleep(rand.nextInt(50));
                publish(nC++);
               // break;
              
                //nC++;
            }
         
          
            return null;
        }

        @Override
        protected void done() {
           // super.done(); //To change body of generated methods, choose Tools | Templates.

            System.out.println("");
            if (jpanels.size() > 0) {

                mainPanel.remove(bar.getPanel());
            }

            mainPanel.updateUI();
        }

        @Override
        protected void process(final List<Integer> chunks) {
            //super.process(chunks); //To change body of generated methods, choose Tools | Templates.
            // int nVal = chunks.get(0)
         bar.getBar().setValue(chunks.get(0));
          //  mainPanel.updateUI();

           // setProgress(chunks.get(0));
//            SwingUtilities.invokeLater(new Runnable() {
//                public void run() {
//                 setProgress(chunks.get(0));
//                  //  bar.setValue(chunks.get(0));
//
//                }
//            });

        }

    }
    
//    private void createAddWorkers(){
//              for (int i = 0; i < 30; i++) {
//            new AddWorker(jpanels, mainPanel).execute();
//          // new RemoveWorker(jpanels, mainPanel).execute();
//
//        }
//    }

        class MyBar {
            private JPanel panel;
            private JProgressBar bar;
//            private java.util.UUID uuid;

        public MyBar(JPanel panel, JProgressBar bar ) {
            this.panel = panel;
            this.bar = bar;
//            this.uuid = uuid;
        }

//        public UUID getUuid() {
//            return uuid;
//        }
//
//        public void setUuid(UUID uuid) {
//            this.uuid = uuid;
//        }
        
        

        public JPanel getPanel() {
            return panel;
        }

        public void setPanel(JPanel panel) {
            this.panel = panel;
        }

        public JProgressBar getBar() {
            return bar;
        }

        public void setBar(JProgressBar bar) {
            this.bar = bar;
        }
            
            
        }
        
       class AddWorker extends SwingWorker<Void, Void> {

        private ArrayList<MyBar> myAddBars;
       // private JPanel mainPanel;
        private Random rand;

        public AddWorker(ArrayList<MyBar> myAddBars) {

            this.myAddBars = myAddBars;
            //this.mainPanel = mainPanel;
            rand = new Random();
        }

        @Override
        protected Void doInBackground() throws Exception {
           // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

            Thread.sleep(rand.nextInt(10 * 1000));

            return null;
        }

        @Override
        protected void done() {
            // super.done(); //To change body of generated methods, choose Tools | Templates.
            GridBagConstraints gbc = new GridBagConstraints();
            JLabel label = new JLabel("Progress (" + nY + ")");
            //JTextField text = new JTextField(15);
            
            JProgressBar progresBar = new JProgressBar();

            //create a new JPanel and add components to this
            JPanel mini = new JPanel();
            mini.add(label);
            mini.add(progresBar);
            jpanels.add(mini);
            MyBar mBar = new MyBar(mini, progresBar);
            myBars.add(mBar);

            gbc.gridx = GridBagConstraints.FIRST_LINE_START;
            gbc.gridy = nY++;
            
           

            mainPanel.add(jpanels.get(jpanels.size() - 1), gbc);
             new  SimulateProgressWorker(myBars, mBar).execute();
            mainPanel.updateUI();
            
           
            
        }

    }

      class RemoveWorker extends SwingWorker<Void, Void> {

        private ArrayList<JPanel> jpanels;
        private JPanel mainPanel;
        private Random rand;

        public RemoveWorker(ArrayList<JPanel> jpanels, JPanel mainPanel) {

            this.jpanels = jpanels;
            this.mainPanel = mainPanel;
            rand = new Random();
        }

        @Override
        protected Void doInBackground() throws Exception {
           // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

            Thread.sleep(rand.nextInt(10 * 2000));

            return null;
        }

        @Override
        protected void done() {
            if (jpanels.size() > 0) {
                JPanel removePanel = jpanels.remove(0);
                mainPanel.remove(removePanel);
            }

            mainPanel.updateUI();
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 439, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManyProgs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManyProgs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManyProgs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManyProgs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ManyProgs().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel mainPanel;
    // End of variables declaration//GEN-END:variables
}
